#!/bin/bash

maindir=`pwd`
for sericedir in `find .  -name "pom.xml" -exec dirname {} \; | grep service`
do 
echo  $sericedir
cd $sericedir
servicename=${PWD##*/}
echo $servicename
 nohup mvn compile package spring-boot:run  1>$maindir/$servicename.out  2>$maindir/$servicename.err &
cd $maindir

done

