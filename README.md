# microservices-reference-architecture #

This is the reference architecture for a distributed cloud ready micro service platform

## Technologies ##

* Spring Boot
* Spring Boot Cloud
* Netflix OSS Libraires ( included in spring cloud)
	* Hystrix
	* Feign
	* Ribbon
	* Zuul
	* Eureka
* Swagger 2
* Maven
* Docker

## How do I get set up? ##

### Check out the repo ###

`git clone https://kanzifucius@bitbucket.org/kanzifucius/microservices-reference-architecture.git`

### Check out the repo ###
Teh following depenancies are needed

1. MongoDB
2. JDK 1.8
3. Maven 3 +
4. Docker

## Staring ##


* Run the Following in order using the maven command
`mvn package spring-boot:run`

1. `msrefarh-service-eurkaserver`
2. `msrefarch-service-config`
3. `msrefarch-service-auth`
5. `msrefarch-service-users`
5. `msrefarch-service-apigateway`
6. `msrefarch-service-edgeservice`

## Security OAuth2##
The services are secured with OAuth2. The Oauth2 serviced `msrefarch-service-auth` is responsible for
oauth authentication that is used by all the services.

The `msrefarch-service-edgeservice` is configured to authenticated using a configured client and api. This
is then propagated downward to services that are called using the bearer token acquired from the `msrefarch-service-auth` service.

`client_id : apicleint`
`Password  : apicleintsecret`

Basic authentication is used to secure the White label OAuth login form.
`user : apiuser`
`password :apipassword`

For more information see the readme for the `msrefarch-service-auth` project.




## Service Endpoint Inspection ##

All services will share common endpoints.
All these endpoints can be inspected using swagger.

`http://{server}:{port}/swagger-ui.html`

**hystrix-stream-endpoint:** 
This is the endpoint exposing the hystix data stream for the service

**environment-mvc-endpoint :**
This is the endpoint to retrieve the environment properties set of the service

**metrics-mvc-endpoin:**
This is the end point for retrieve metric for the service provided for by spring actuator
   