start /D    Infrastructure\msrefarch-service-config  mvn  compile package spring-boot:run -Dmaven.test.skip=true
start /D    Infrastructure\msrefarh-service-eurkaserver  mvn  compile package spring-boot:run -Dmaven.test.skip=true
start /D    Infrastructure\msrefarch-service-auth  mvn  compile package spring-boot:run -Dmaven.test.skip=true
start /D    Infrastructure\msrefarch-service-edgeservice   mvn compile package spring-boot:run -Dmaven.test.skip=true
start /D    CoreServices\msrefarch-service-users  mvn  compile package spring-boot:run -Dmaven.test.skip=true
start /D    ApiServices\msrefarch-service-apigateway  mvn compile package spring-boot:run -Dmaven.test.skip=true
