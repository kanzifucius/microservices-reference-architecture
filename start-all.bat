start /D    Infrastructure\msrefarh-service-eurkaserver  mvn  compile package spring-boot:run
start /D    Infrastructure\msrefarch-service-config  mvn  compile package spring-boot:run
start /D    Infrastructure\msrefarch-service-auth  mvn  compile package spring-boot:run
start /D    Infrastructure\msrefarch-service-hystrixdashboard  mvn compile package spring-boot:run
start /D    Infrastructure\msrefarch-service-edgeservice   mvn compile package spring-boot:run
start /D    ApiServices\msrefarch-service-apigateway  mvn compile package spring-boot:run
start /D   Infrastructure\msrefarch-service-springbootadmin  mvn compile package spring-boot:run