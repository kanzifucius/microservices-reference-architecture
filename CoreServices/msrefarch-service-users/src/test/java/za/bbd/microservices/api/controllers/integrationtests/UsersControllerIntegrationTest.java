package za.bbd.microservices.api.controllers.integrationtests;

import com.jayway.restassured.RestAssured;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import za.bbd.microservices.UsersServiceApplication;

import static com.jayway.restassured.RestAssured.when;

/**
 * Created by facilities on 6/12/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = UsersServiceApplication.class)
@WebIntegrationTest(value = "server.port:0")
public class UsersControllerIntegrationTest {


    @Value("${local.server.port}")
    // 6
            int port;

    @Before
    public void setUp() {
        RestAssured.port = port;
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();

    }

    @Test
    public void getItemsShouldReturnBothItems() {
        when().
                get("/users").
                then().
                statusCode(HttpStatus.SC_OK).
                body("userName", Matchers.hasItems("Test"));
    }


}
