package za.bbd.microservices.api.controllers.tests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import za.bbd.microservices.UsersServiceApplication;

/**
 * Created by facilities on 6/12/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = UsersServiceApplication.class)
@WebAppConfiguration
public class UsersControllerTest {


    @Test
    public void canGetUsers() {


        int x = 1;
        Assert.assertTrue("No Users Returned", x == 0);

    }

}
