package za.bbd.microservices.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.security.oauth2.resource.EnableOAuth2Resource;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.bind.annotation.*;
import za.bbd.microservices.domain.User;
import za.bbd.microservices.repositories.UserRepo;

/**
 * Created by facilities on 6/10/15.
 */
@RestController
@EnableOAuth2Resource
public class UsersController  {

    @Autowired
    UserRepo userRepo;


    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public Iterable<User> GetAllUsers() {
        return userRepo.findAll();

    }


    @RequestMapping(value = "/users/{Id}", method = RequestMethod.GET)
    public Iterable<User> GetUser(@PathVariable String Id) {
        return userRepo.findById(Id);
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity<User> CreateUser(@RequestBody User user) {
        userRepo.save(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }



}
