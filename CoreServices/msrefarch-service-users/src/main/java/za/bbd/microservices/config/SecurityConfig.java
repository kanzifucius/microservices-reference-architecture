package za.bbd.microservices.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * Created by facilities on 7/30/15.
 */
public class SecurityConfig
{

    @Configuration
    protected static class ResourceSecurityConfigurer extends ResourceServerConfigurerAdapter {



        @Override
        public void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests()
                    .antMatchers("/swagger-ui.html","/webjars/**","/configuration/**","/swagger-resources","/v2/api-docs/**").permitAll().anyRequest()
                    .authenticated();
        }

    }
}
