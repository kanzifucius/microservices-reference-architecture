package za.bbd.microservices.repositories;

import za.bbd.microservices.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 */
public interface UserRepo extends MongoRepository<User, String> {
    Iterable<User> findById(String id);
}
