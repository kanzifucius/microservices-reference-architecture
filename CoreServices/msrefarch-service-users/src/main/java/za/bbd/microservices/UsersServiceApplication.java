package za.bbd.microservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * <h1>UsersServiceApplication</h1>
 * This is the main spring class for the spring boot application
  * @author Ricardo Pinto
 */
@SpringBootApplication
@EnableMongoRepositories
@EnableDiscoveryClient
@EnableSwagger2
public class UsersServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(UsersServiceApplication.class, args);
    }


}
