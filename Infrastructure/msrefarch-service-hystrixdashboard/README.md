# Spring boot hystix dashboard application #
This is a simple spring boot services that is annoted with `@EnableHystrixDashboard`.
This exposes an endpoint  `http://{host}:{port}/hystrix'`
This is a sdashboad where you can input a hystrix stream exposed by services to get the ciruit breaker stats

# Streams that can be used in the dashboard  #
Cluster via Turbine (default cluster): http://turbine-hostname:port/turbine.stream
Cluster via Turbine (custom cluster): http://turbine-hostname:port/turbine.stream?cluster=[clusterName]
Single Hystrix App: http://hystrix-app:port/hystrix.stream`
