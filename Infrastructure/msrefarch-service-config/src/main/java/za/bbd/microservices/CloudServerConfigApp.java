package za.bbd.microservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * Main cloud server configuration application
 */
@SpringBootApplication
@EnableConfigServer
public class CloudServerConfigApp {
    public static void main(String[] args){
        SpringApplication.run(CloudServerConfigApp.class, args);
    }
}
