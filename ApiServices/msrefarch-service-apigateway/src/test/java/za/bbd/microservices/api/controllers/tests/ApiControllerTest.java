package za.bbd.microservices.api.controllers.tests;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import za.bbd.microservices.api.ApiGatewayApplicaiton;
import za.bbd.microservices.api.services.User;
import za.bbd.microservices.api.services.UsersService;

import java.util.List;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;

/**
 * Created by facilities on 6/11/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApiGatewayApplicaiton.class)
public class ApiControllerTest {

    @Autowired
    UsersService usersService;

    @Before
    public void setUp() {

    }

    @Test
    public void canGetUsers() {

        List<User> users = usersService.GetUsers();

        Assert.assertTrue("No Users Returned", users.size() > 0);
        assertThat(users, hasItem(hasProperty("userName", is("YourValue"))));
    }


}
