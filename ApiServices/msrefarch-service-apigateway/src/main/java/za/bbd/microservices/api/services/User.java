package za.bbd.microservices.api.services;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by facilities on 6/10/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {


    private String id;

    private String mlId;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    private String userName;
}
