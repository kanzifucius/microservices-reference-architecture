package za.bbd.microservices.api.controllers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.security.oauth2.resource.EnableOAuth2Resource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import za.bbd.microservices.api.services.User;
import za.bbd.microservices.api.services.UsersService;

import java.util.List;

/**
 * Created by facilities on 6/10/15.
 */
@RestController
@EnableOAuth2Resource
public class ApiController {

    Log log = LogFactory.getLog(ApiController.class);

    @Autowired
    UsersService usersService;

    @RequestMapping(value ="/users", method = RequestMethod.GET)
    public List<User> GetUsersApi() throws Exception{

        log.debug("Loading Call GetUsers APi");
        return usersService.GetUsers();

    }





}
