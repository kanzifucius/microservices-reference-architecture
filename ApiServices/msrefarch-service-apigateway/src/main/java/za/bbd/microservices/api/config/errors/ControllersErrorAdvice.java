package za.bbd.microservices.api.config.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Contoller advice for exception handling in all controllers for the project
 * @author Ricardo Pinto
 * */
@ControllerAdvice
public class ControllersErrorAdvice {

    /**
     * Exception handler method for IllegalArgumentException
     * @param response
     * @throws java.io.IOException
     */
    @ExceptionHandler(IllegalArgumentException.class)
    void handleBadRequests(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), "Invalid Parameters");
    }
}
