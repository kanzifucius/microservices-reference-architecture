package za.bbd.microservices.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**<h1>ApiGatewayApplicaiton</h1>
 *This is the main spring

 class for the spring boot application
 * This is the edge service api gateway.
 * This service would be the api service that is the course grained serveries of composing calls to all other services
 * @author Ricardo Pinto
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@EnableHystrix
@EnableSwagger2
public class ApiGatewayApplicaiton {

    public static void main(String[] args) {
        SpringApplication.run(ApiGatewayApplicaiton.class, args);
    }




}
