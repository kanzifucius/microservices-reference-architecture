package za.bbd.microservices.api.services;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.bbd.microservices.api.serviceclients.UsersServiceClient;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;


/**
 * Created by facilities on 6/10/15.
 */
@Service
public class UsersService {
    @Autowired
    UsersServiceClient usersServiceClient;

    Log log = LogFactory.getLog(UsersService.class);

    @HystrixCommand(fallbackMethod = "GetUsersFallBack")
    public List<User> GetUsers() {

        log.debug("Calling Feign Cleint");
      return  usersServiceClient.getUsers();

    }


    public List<User> GetUsersFallBack() {
        User userStub = new User();
        userStub.setUserName("FALL_BACK");
        return Arrays.asList(userStub);
    }

}
