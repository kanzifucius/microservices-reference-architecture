package za.bbd.microservices.api.serviceclients;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.bbd.microservices.api.services.User;

import java.util.List;

/**
 * Created by facilities on 6/11/15.
 */
@FeignClient("msrefarch-service-users")
public  interface UsersServiceClient {

    @RequestMapping(method = RequestMethod.GET, value = "/users")
    List<User> getUsers();

}
